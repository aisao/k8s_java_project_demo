package com.app.service;

import com.app.conf.AppConfig;
import com.app.util.tools.ThreadCache;

public class ServiceA
{

	public void hello()
	{
		ThreadCache cache = AppConfig.getAppConfig().getCache();	
//		System.out.println("ServiceA->hello:"+cache.get());
		cache.addln("ServiceA/");
		cache.get();
	}
}
