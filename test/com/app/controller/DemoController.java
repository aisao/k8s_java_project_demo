package com.app.controller;

import java.util.regex.Pattern;

import com.app.annotation.UrlMapper;
import com.app.conf.AppConfig;
import com.app.service.ServiceA;
import com.app.service.ServiceB;
import com.app.util.tools.ThreadCache;

@UrlMapper(val="/demo")
public class DemoController extends BaseController
{
	
	private ThreadCache cache = AppConfig.getAppConfig().getCache();
	
	public void index()
	{
		
		System.out.println("ID:"+Thread.currentThread().getId());
		
		renderText("JFinal->DemoController->IndexAction");
	}
	
	
	public void demo()
	{
		cache.addln("snake:"+System.currentTimeMillis());
		
		new ServiceA().hello();
		
		new ServiceB().hello();
		
		renderText(cache.addln("JFinal->DemoController->IndexAction"));
	}
	
	public void count()
	{
		ThreadCache cache = AppConfig.getAppConfig().getCache();
		
		renderText("count:"+cache.count());
	}
	
	public void get()
	{
		ThreadCache cache = AppConfig.getAppConfig().getCache();
		
		renderText("value:"+cache.get());
	}
	
	public void paramget()
	{
	    renderText("paramget");
	}
	
    public void parampost()
    {
        //filedemo
        
//        UploadFile  file = getFile("filedemo");
//        System.out.println(file.getUploadPath());
//        
        renderText("parampost");
    }
	   
	public void paramjson()
	{
        renderText("paramjson");
	}
	    
	public void paramgetpost()
	{
        renderText("paramgetpost");
	}
	
	public void snake()
	{
	    renderText(getPara("test")+"/"+getPara("name"));
	}
	
	public void regex()
	{
	    //汉字 大写英文 小写英文 数字 : - [ ] { } ,
//	    String regex = "^[\u4e00-\u9fa5 A-Z a-z 0-9 \\: \\- \\[ \\] \" \\{ \\} \\,]+$";
	    String regex = "^[\u4e00-\u9fa5 A-Z a-z 0-9 : , { } \" \\- \\[ \\] ]+$";
//	    String regex = "^\\\"+$";
	    
	    renderText("boolean:"+Pattern.matches(regex,getPara("test","test")));
	}
}
