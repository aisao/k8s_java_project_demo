package com.app.conf.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Conf
{
    /**
     * class文件所在的目录
     * **/
    public final static String classpath = "bin";

	/**
	 * 网站根目录
	 * **/
	public static String www     = "/pub";
	
    /**
     * 是否展示SQL
     * **/
	private static boolean showSql = true;
	
	/**
	 * 是否显示SQL
	 * **/
	public static boolean isShowSql()
	{
		return showSql;
	}
	
    /**
     * 数据库
     * **/
    private static List<Map<String,String>> dbs = new ArrayList<Map<String,String>>();
    
    private static Map<String,String> db_00     =  new HashMap<String,String>();
	
	static
	{
		//默认数据库
		db_00.put("name", "");
		db_00.put("type", "");
		db_00.put("className", "com.mysql.jdbc.Driver");
		db_00.put("jdbcUrl", "");
		db_00.put("userName", "");
		db_00.put("password", "");
		dbs.add(db_00);
	}
	
	/**
	 * 获取数据库配置
	 * **/
	public static List<Map<String,String>> getDbs()
	{
		return dbs;
	}
}
