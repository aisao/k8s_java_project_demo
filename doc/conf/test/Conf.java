package com.app.conf.info;

public class Conf
{
    /**
     * class文件所在的目录
     * **/
    public final static String classpath = "bin";

	/**
	 * 网站根目录
	 * **/
	public static String www     = "/pub";

}
