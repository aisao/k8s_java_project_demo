package com.app.controller;

import com.app.annotation.UrlMapper;
import com.app.util.http.HttpClient;
import com.app.util.http.HttpResult;
import com.jfinal.core.Controller;

@UrlMapper(val="/istio")
public class IstioController extends Controller
{
	public void index()
	{
		renderText("JFinal->IndexController->IndexAction");
	}
	
	public void d00()
	{
		HttpClient client = new HttpClient();
		HttpResult result = client.get("http://k8s-java-d01.demo1:12001/istio/d01", null);
		
		renderText("JFinal->istio->d00->"+new String(result.getBody()));
	}
	
	public void d01()
	{
		HttpClient client = new HttpClient();
		HttpResult result = client.get("http://k8s-java-d02.demo2:12002/istio/d02", null);
		
		renderText("JFinal->istio->d01->"+new String(result.getBody()));
	}
	
	public void d02()
	{
		renderText("JFinal->istio->d02");
	}
	
	
}
