package com.app.controller;

import java.util.Enumeration;

import com.app.annotation.UrlMapper;

@UrlMapper(val="/")
public class RootController extends BaseController
{
	public void index() throws Exception
	{

	    String key               = null;
	    Enumeration<String> list = getRequest().getHeaderNames();
	    
	    while(list.hasMoreElements())
	    {
	        key = list.nextElement();
	        System.out.println(key+":"+getRequest().getHeader(key));
	    }


		renderText("JFinal->RootController->IndexAction");
	}
}
