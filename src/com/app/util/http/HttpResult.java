package com.app.util.http;

import java.util.Map;

/**
 * 返回值
 * **/
public class HttpResult 
{
	private int code = 0;
	
	private byte[] body;

	private Map<String,String> responseHeaders;
	
	public int getCode() 
	{
		return code;
	}

	public HttpResult setCode(int code) 
	{
		this.code = code;
		return this;
	}

	public byte[] getBody() 
	{
		return body;
	}

	public HttpResult setBody(byte[] body)
	{
		this.body = body;
		return this;
	}

	public Map<String, String> getResponseHeaders() 
	{
		return responseHeaders;
	}

	public HttpResult setResponseHeads(Map<String, String> responseHeaders) 
	{
		this.responseHeaders = responseHeaders;
		return this;
	}
	
}