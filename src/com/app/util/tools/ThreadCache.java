package com.app.util.tools;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 简易线程缓存信息
 * **/
public class ThreadCache 
{
	private Map<Long,String> threadCache = new ConcurrentHashMap<Long,String>();
	
	/**
	 * 获取缓存信息
	 * **/
	public String get()
	{
		return threadCache.get(Thread.currentThread().getId());
	}
	
	/**
	 * 设置缓存信息,覆盖原有信息
	 * **/
	public String set(String value)
	{
		threadCache.put(Thread.currentThread().getId(), value);
		return value;
	}
	
	/**
	 * 设置缓存信息,累加原有信息
	 * **/
	public String add(String value)
	{
		String cache = this.get();
		
		if(null == cache)
		{
			this.set(value);
		}
		else
		{
			this.set(cache+value);
		}
		
		return value;
	}
	
	/**
	 * 设置缓存信息,累加原有信息,自动换行
	 * **/
	public String addln(String value)
	{
		this.add(value+"\r\n");
		return value;
	}
	
	/**
	 * 获取缓存信息
	 * **/
	public int count()
	{
		return threadCache.size();
	}
	
	/**
	 * 删除缓存信息
	 * **/
	public void del()
	{
		threadCache.remove(Thread.currentThread().getId());
	}
}