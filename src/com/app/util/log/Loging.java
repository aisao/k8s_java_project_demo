package com.app.util.log;

import java.io.File;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Loging 
{
	private Logger logger;
	
	private static Loging loging;
	
	/**
	 * 日志名字
	 * **/
	public synchronized static Loging getLoging(String name, int limit)
	{
		if(null == loging)
		{
			try 
			{
				loging = new Loging(name,limit);
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return loging;
	}
	
	/**
	 * 构建日志类
	 * @param name 日志名称
	 * @param limit 日志大小(byte)
	 * **/
	private Loging(String name,int limit)throws Exception
	{
		//获取工作目录,获取系统目录拼接符
		String root    = System.getProperty("user.dir");
		String sep     = File.separator;
		
		//文件控制
		FileHandler fileHandler = new FileHandler(root+sep+name+".log",limit,1,true);
		fileHandler.setEncoding("UTF-8");
		fileHandler.setFormatter(new LogMsgFormat());
		
		//控制台控制器
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setEncoding("UTF-8");
		consoleHandler.setFormatter(new LogMsgFormat());

		//创建日志
		logger = Logger.getLogger(name);
		logger.setUseParentHandlers(false);
		logger.addHandler(fileHandler);
		logger.addHandler(consoleHandler);
		logger.setLevel(java.util.logging.Level.ALL);
	}
	
	/**
	 * 
	 * **/
	public void finest(String msg)
	{
		//输出日志		
		logger.finest(msg);
	}
	
	/**
	 * 
	 * **/
	public void finer(String msg)
	{
		//输出日志
        logger.finer(msg);
	}
	
	/**
	 * 
	 * **/
	public void fine(String msg)
	{
		//输出日志
        logger.fine(msg);
	}
	
	/**
	 * 
	 * **/
	public void info(String msg)
	{
		//输出日志
        logger.info(msg);
	}
	
	/**
	 * 
	 * **/
	public void config(String msg)
	{
		//输出日志
        logger.config(msg);
	}
	
	/**
	 * 
	 * **/
	public void warning(String msg)
	{
		//输出日志
        logger.warning(msg);
	}
	
	/**
	 * 
	 * **/
	public void severe(String msg)
	{
		//输出日志
        logger.severe(msg);
	}
}