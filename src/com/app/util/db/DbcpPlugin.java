//package com.app.util.db;
//
//import java.sql.SQLException;
//
//import javax.sql.DataSource;
//
//import org.apache.commons.dbcp2.BasicDataSource;
//
//import com.jfinal.plugin.IPlugin;
//import com.jfinal.plugin.activerecord.IDataSourceProvider;
//
//public class DbcpPlugin implements IPlugin, IDataSourceProvider
//{
//
//    private String url;
//    private String userName;
//    private String password;
//    private String driverClassName = "com.mysql.jdbc.Driver";
//    
//    /**
//     * 最大的空闲值
//     * **/
//    private int maxIdle = 10;
//    
//    /**
//     * 最小的空闲值
//     * **/
//    private int minIdle = 5;
//    
//    /**
//     * 初始化数量
//     * **/
//    private int initialSize = 20;
//    
//    /**
//     * 回收期运行间隔(毫秒)
//     * **/
//    private long timeBetweenEvictionRunsMillis = 30000;
//    
//    /**
//     * 设置是否合并语句
//     * **/
//    private boolean poolingStatements = true;
//    
//    /**
//     * 最大声明数量
//     * **/
//    private int maxOpenStatements = 200;
//    
//    /**
//     * 设置删除放弃的连接之前的超时时间（秒）
//     * **/
//    private int removeAbandonedTimeout = 10;
//    
//    /**
//     * 申请连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    private boolean testOnBorrow;
//    
//    /**
//     * 归还连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    private boolean testOnReturn;
//    
//    /**
//     * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于 timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
//     * **/
//    private boolean testWhileIdle;
//    
//    /**
//     * 用来检测连接是否有效的sql，要求是一个查询语句,如果validationQuery为 null，testOnBorrow、testOnReturn、testWhileIdle都不起其作用。
//     * **/
//    private String validationQuery;
//    
//    /**
//     * 是否默认提交false开始事物
//     * **/
//    private boolean defaultAutoCommit = true;
//    
//    /**
//     * 数据源
//     * **/
//    private BasicDataSource dataSource;
//    
//    /**
//     * 是否已启动
//     * **/
//    private boolean isStarted = false;
//    
//    public DbcpPlugin(String jdbcUrl, String user, String password)
//    {
//        this.url      = jdbcUrl;
//        this.userName = user;
//        this.password = password;
//    }
//    
//    @Override
//    public DataSource getDataSource()
//    {
//        return dataSource;
//    }
//
//    @Override
//    public boolean start()
//    {
//        if (isStarted)
//            return true;
//        
//        dataSource =   new BasicDataSource();
//        
//        dataSource.setUrl(url);
//        dataSource.setDriverClassName(driverClassName);
//        dataSource.setUsername(userName);
//        dataSource.setPassword(password);
//        
//        dataSource.setMaxIdle(maxIdle);
//        dataSource.setMinIdle(minIdle);
//        dataSource.setInitialSize(initialSize);
//        dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
//        
//        dataSource.setPoolPreparedStatements(poolingStatements);
//        dataSource.setMaxOpenPreparedStatements(maxOpenStatements);
//        dataSource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
//        
//        dataSource.setTestOnBorrow(testOnBorrow);
//        dataSource.setTestOnReturn(testOnReturn);
//        dataSource.setTestWhileIdle(testWhileIdle);
//        dataSource.setValidationQuery(validationQuery);
//        
//        dataSource.setDefaultAutoCommit(defaultAutoCommit);
//        isStarted = true;
//        return true;
//    }
//
//    @Override
//    public boolean stop()
//    {
//        try
//        {
//            if (dataSource != null)
//                dataSource.close();
//        }
//        catch (SQLException e)
//        {
//            e.printStackTrace();
//        }
//        finally
//        {
//            dataSource = null;
//            isStarted = false;
//        }
//        
//        return true;
//    }
//
//    /**
//     * 数据库驱动
//     * **/
//    public String getDriverClassName()
//    {
//        return driverClassName;
//    }
//
//    /**
//     * 数据库驱动
//     * **/
//    public void setDriverClassName(String driverClassName)
//    {
//        this.driverClassName = driverClassName;
//    }
//
//    /**
//     * 最大的空闲值
//     * **/
//    public int getMaxIdle()
//    {
//        return maxIdle;
//    }
//
//    /**
//     * 最大的空闲值
//     * **/
//    public void setMaxIdle(int maxIdle)
//    {
//        this.maxIdle = maxIdle;
//    }
//
//    /**
//     * 最小的空闲值
//     * **/
//    public int getMinIdle()
//    {
//        return minIdle;
//    }
//
//    /**
//     * 最小的空闲值
//     * **/
//    public void setMinIdle(int minIdle)
//    {
//        this.minIdle = minIdle;
//    }
//
//    /**
//     * 初始化数量
//     * **/
//    public int getInitialSize()
//    {
//        return initialSize;
//    }
//
//    /**
//     * 初始化数量
//     * **/
//    public void setInitialSize(int initialSize)
//    {
//        this.initialSize = initialSize;
//    }
//
//    /**
//     * 回收期运行间隔(毫秒)
//     * **/
//    public long getTimeBetweenEvictionRunsMillis()
//    {
//        return timeBetweenEvictionRunsMillis;
//    }
//
//    /**
//     * 回收期运行间隔(毫秒)
//     * **/
//    public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis)
//    {
//        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
//    }
//
//    /**
//     * 设置是否合并语句
//     * **/
//    public boolean isPoolingStatements()
//    {
//        return poolingStatements;
//    }
//
//    /**
//     * 设置是否合并语句
//     * **/
//    public void setPoolingStatements(boolean poolingStatements)
//    {
//        this.poolingStatements = poolingStatements;
//    }
//
//    /**
//     * 最大声明数量
//     * **/
//    public int getMaxOpenStatements()
//    {
//        return maxOpenStatements;
//    }
//
//    /**
//     * 最大声明数量
//     * **/
//    public void setMaxOpenStatements(int maxOpenStatements)
//    {
//        this.maxOpenStatements = maxOpenStatements;
//    }
//
//    /**
//     * 设置删除放弃的连接之前的超时时间（秒）
//     * **/
//    public int getRemoveAbandonedTimeout()
//    {
//        return removeAbandonedTimeout;
//    }
//
//    /**
//     * 设置删除放弃的连接之前的超时时间（秒）
//     * **/
//    public void setRemoveAbandonedTimeout(int removeAbandonedTimeout)
//    {
//        this.removeAbandonedTimeout = removeAbandonedTimeout;
//    }
//
//    /**
//     * 申请连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    public boolean isTestOnBorrow()
//    {
//        return testOnBorrow;
//    }
//
//    /**
//     * 申请连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    public void setTestOnBorrow(boolean testOnBorrow)
//    {
//        this.testOnBorrow = testOnBorrow;
//    }
//
//    /**
//     * 归还连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    public boolean isTestOnReturn()
//    {
//        return testOnReturn;
//    }
//
//    /**
//     * 归还连接时执行validationQuery检测连接是否有效，配置为true会降低性能
//     * **/
//    public void setTestOnReturn(boolean testOnReturn)
//    {
//        this.testOnReturn = testOnReturn;
//    }
//
//    /**
//     * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于 timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
//     * **/
//    public boolean isTestWhileIdle()
//    {
//        return testWhileIdle;
//    }
//
//    /**
//     * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于 timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
//     * **/
//    public void setTestWhileIdle(boolean testWhileIdle)
//    {
//        this.testWhileIdle = testWhileIdle;
//    }
//
//    /**
//     * 用来检测连接是否有效的sql，要求是一个查询语句,如果validationQuery为 null，testOnBorrow、testOnReturn、testWhileIdle都不起其作用。
//     * **/
//    public String getValidationQuery()
//    {
//        return validationQuery;
//    }
//
//    /**
//     * 用来检测连接是否有效的sql，要求是一个查询语句,如果validationQuery为 null，testOnBorrow、testOnReturn、testWhileIdle都不起其作用。
//     * **/
//    public void setValidationQuery(String validationQuery)
//    {
//        this.validationQuery = validationQuery;
//    }
//
//    /**
//     * 自动提交commit
//     * **/
//    public boolean isDefaultAutoCommit()
//    {
//        return defaultAutoCommit;
//    }
//
//    /**
//     * 自动提交commit
//     * **/
//    public void setDefaultAutoCommit(boolean defaultAutoCommit)
//    {
//        this.defaultAutoCommit = defaultAutoCommit;
//    }
//    
//}