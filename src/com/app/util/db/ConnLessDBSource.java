package com.app.util.db;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

/**
 * 多库数据源,创建时不会预先创建数据库连接
 * 基于JFinal使用,同数据源操作多个数据库时候,只能在默认数据源下进行
 * **/
public class ConnLessDBSource implements DataSource
{
    /**
     * 线程缓存,用于保存连接数据的url user pass
     * **/
    private ThreadLocal<SourceDb> tCache = new ThreadLocal<SourceDb>();

    /**
     * 数据库选择器,用于切换tCache中的数据库连接信息
     * **/
    private SelectDb selectDb = new SelectDb();

    /**
     * 数据库选择器
     * **/
    public SelectDb getSelectDb()
    {
        return selectDb;
    }

    /**
     * 初始化数据源,并加载数据库驱动
     * **/
    public ConnLessDBSource(String clzzName)
    {
        try
        {
            Class.forName(clzzName);
        } 
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前数据源的线程缓存
     * **/
    public ThreadLocal<SourceDb> gettCache()
    {
        return tCache;
    }

    /**
     * 获取连接
     * **/
    @Override
    public Connection getConnection() throws SQLException
    {
        SourceDb db = tCache.get();

        //选择默认库
        if(null == db)
        {
            selectDb.use();db = tCache.get();
        }

        if(null == db)
        {
            throw new SQLException("未能获取到可用的数据库信息");
        }

        DriverManager.setLoginTimeout(5);//暂定5秒
        return DriverManager.getConnection(db.getUrl(), db.getUserName(), db.getPassword());
    }

    /**
     * 需要实现
     * **/
    @Override
    public PrintWriter getLogWriter() throws SQLException
    {
        throw new SQLException("不支持PrintWriter");
    }

    /**
     * 需要实现
     * **/
    @Override
    public void setLogWriter(PrintWriter out) throws SQLException
    {
        throw new SQLException("不支持PrintWriter");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public Connection getConnection(String username, String password) throws SQLException
    {
        throw new UnsupportedOperationException("dbcp就是这么写的:Not supported by BasicDataSource");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public void setLoginTimeout(int seconds) throws SQLException
    {
        throw new UnsupportedOperationException("dbcp就是这么写的:Not supported by BasicDataSource");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public int getLoginTimeout() throws SQLException
    {
        throw new UnsupportedOperationException("dbcp就是这么写的:Not supported by BasicDataSource");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException
    {
        throw new SQLFeatureNotSupportedException("dbcp就是这么写的");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        throw new SQLException("dbcp就是这么写的:BasicDataSource is not a wrapper.");
    }

    /**
     * 参照dbcp
     * **/
    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException
    {
        return false;
    }

}