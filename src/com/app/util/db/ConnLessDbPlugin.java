package com.app.util.db;


import javax.sql.DataSource;

import com.jfinal.plugin.IPlugin;
import com.jfinal.plugin.activerecord.IDataSourceProvider;

/**
 * 无连接数据源插件
 * **/
public class ConnLessDbPlugin implements IPlugin, IDataSourceProvider
{
    //
    private ConnLessDBSource multiDBSource;
    
    //
    private String driverClassName;
    
    //
    private boolean isStarted = false;
    
    public ConnLessDbPlugin setDriverClassName(String driverClassName)
    {
        this.driverClassName = driverClassName;
        return this;
    }
    
    /**
     * 获取数据源
     * **/
    public DataSource getDataSource()
    {
        return multiDBSource;
    }

    /**
     * 启动数据源
     * **/
    public boolean start()
    {
        if (isStarted)
            return true;

      try
      {
          multiDBSource = new ConnLessDBSource(driverClassName);
          return isStarted = true;
      } 
      catch (Exception e)
      {
          e.printStackTrace();
          return isStarted = false;
      }
    }

    /**
     * 无连接数据源不存在保持的长连接默认返回true无其他操作
     * **/
    public boolean stop(){return true;}
    
    /**
     * 数据库选择器
     * **/
    public SelectDb getSelectDb()
    {
        return multiDBSource.getSelectDb();
    }

}