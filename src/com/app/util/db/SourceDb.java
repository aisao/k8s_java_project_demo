package com.app.util.db;

/**
 * 数据源,用于传输对应的数据库信息
 * **/
public class SourceDb
{
    private String url;
    
    private String userName;
    
    private String password;

    public String getUrl()
    {
        return url;
    }

    public SourceDb setUrl(String url)
    {
        this.url = url;
        return this;
    }

    public String getUserName()
    {
        return userName;
    }

    public SourceDb setUserName(String userName)
    {
        this.userName = userName;
        return this;
    }

    public String getPassword()
    {
        return password;
    }

    public SourceDb setPassword(String password)
    {
        this.password = password;
        return this;
    }
}