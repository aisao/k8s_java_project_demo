package com.app.util.db;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.activerecord.DbKit;

/**
 * 数据库选择器
 * **/
public class SelectDb
{
    private Map<String,SourceDb> dbs = new HashMap<String,SourceDb>();
    
    /**
     * 默认数据库名称
     * **/
    private String dbsDefault = "";
    
    /**
     * 切换指定的数据库
     * @param name 数据源名称
     * **/
    public SelectDb use(String name)
    {
        ConnLessDBSource multiDBSource = (ConnLessDBSource)DbKit.getConfig(dbsDefault).getDataSource();
        multiDBSource.gettCache().set(dbs.get(name));
        return this;
    }

    /**
     * 切换默认数据库
     * **/
    public SelectDb use()
    {
        return use(dbsDefault);
    }

    /**
     * 添加数据库,首个被添加进来的数据库会被当做默认源使用
     * @param name 数据库名称
     * @param sourceDb 数据库
     * **/
    public SelectDb add(String name,SourceDb sourceDb)
    {
        //名称不可重复
        if(dbs.containsKey(name))
            return this;
        
        if(dbs.size() == 0)
        {
            dbsDefault = name;
            dbs.put(dbsDefault,sourceDb);
        }
        else
        {
            dbs.put(name,sourceDb);
        }

        return this;
    }

    public String getDbsDefault()
    {
        return dbsDefault;
    }

}
